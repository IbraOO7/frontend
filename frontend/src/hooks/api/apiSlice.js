import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { logOut, setCredentials } from '../../redux/auth/authSlice';
import { endpoint } from '../../utils/endpoint';

const baseQuery = fetchBaseQuery({
    baseUrl: `${endpoint}`,
    credentials: 'include',
    prepareHeaders: (headers, {getState}) => {
        const token = getState().auth.token;
        if(token) {
            headers.set("authorization", `bearer ${token}`);
        }
        return headers
    }
});

const baseQueryWithReauth = async (args, api, extraOptions) => {
    let result = await baseQuery(args, api, extraOptions);
    if(result?.error?.originalStatus === 403) {
        console.log('Refresh Token');
        const refreshResult = await baseQuery('/refresh', api, extraOptions);
        console.log(refreshResult)
        if(refreshResult?.data) {
            const user = api.getState().auth.user;
            // Simpan token baru
            api.dispatch(setCredentials({...refreshResult.data, user}));
            // Mencoba akses data token kembali
            result = await baseQuery(args, api, extraOptions)
        } else {
            api.dispatch(logOut());
        }
    }
    return result;
}

export const apiSlice = createApi({
    baseQuery: baseQueryWithReauth,
    endpoints: builder => ({})
});