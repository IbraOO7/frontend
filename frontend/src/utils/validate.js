export const Validates = (values) => {
    let temp = {};
    temp.namaPerusahaan = values.namaPerusahaan !== "" ? "" : "Harus diisi";
    temp.alamatPerusahaan = values.alamatPerusahaan !== "" ? "" : "Harus diisi";
    temp.kabKota = values.alamatPerusahaan !== "" ? "" : "Harus diisi";
    temp.provinsi = values.alamatPerusahaan !== "" ? "" : "Harus diisi";
    return Object.values(temp).every(x => x === "");
};
