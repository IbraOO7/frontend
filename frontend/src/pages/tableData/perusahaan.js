/*!
=========================================================
* Muse Ant Design Dashboard - v1.0.0
=========================================================
* Product Page: https://www.creative-tim.com/product/muse-ant-design-dashboard
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/muse-ant-design-dashboard/blob/main/LICENSE.md)
* Coded by Creative Tim
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import {
    Row,
    Col,
    Card,
    Radio,
    Table,
    Input,
    Upload,
    message,
    Progress,
    Button,
    Avatar,
    Typography,
    Tag,
    AutoComplete
  } from "antd";
  import moment from "moment";
  import { Link } from "react-router-dom";
  import React, { useEffect, useState} from "react";
  import { endpoint } from "../../utils/endpoint";
  import axios from "axios";
  import jsPDF from 'jspdf';
  import autoTable from 'jspdf-autotable'
  import * as XLSX from 'xlsx';
  import {isEmpty} from 'lodash';
  // Images
  
  const { Title } = Typography;
  
  // table code start
  
  // project table start  
  function Perusahaan() {
    const onChange = (e) => console.log(`radio checked:${e.target.value}`);
    const [get_data, setGetData] = useState([]);
    const [totalPages, setTotalPages] = useState(1);
    const [sortedInfo, setSortedInfo] = useState({});
    const [loading, setLoading] = useState(false);
    let [filteredData] = useState();
    const [cari, setCari] = useState("");
  
    const dataCompany = async () => {
        setLoading(true);
        await axios.get(`${endpoint}/company/data_perusahaan`)
        .then((res) => {
            setGetData(res.data);
            setTotalPages(res.data.length);
            setLoading(false);
        }).catch((err) => console.log(err));
    }
  
    useEffect(() => {
        dataCompany()
    }, []);

    const dataWithScore = get_data.map((item) => ({
       ...item
    }));

    const modifiedData = dataWithScore.map(({body, ...item}) => ({
        ...item,
        key: item.id,
        message: isEmpty(body) ? item.message: body,
    }));

    const columns = [
      {
        title: 'NO',
        dataIndex: 'No',
        width: '1%',
        key: "No"
      },
      {
        title: "NAMA PERUSAHAAN",
        dataIndex: "name",
        key: "name",
      },
      {
        title: "ALAMAT",
        dataIndex: "function",
        key: "function",
        width: "20%",
      },
      {
        title: "PROVINSI",
        dataIndex: "provinsi",
        key: "provinsi",
      },
      {
        title: "KABUPATEN / KOTA",
        dataIndex: "kabkota",
        key: "kabkota",
      },
      {
        title: "STATUS",
        key: "status",
        dataIndex: "status",
      },
      {
        title: "WAKTU_UJIAN",
        key: "waktu_ujian",
        dataIndex: "waktu_ujian",
      },
    ];

    console.log(get_data);
  
    const data = get_data.map((d, index) => (
      {
        key: `${index+1}`,
        No: (
          <>
              <div className="avatar-info">
                <Title level={5}>{index+1}</Title>
              </div>
          </>
        ),
        name: (
          <>
              <div className="avatar-info">
                <Title level={5}>{d.nama_perusahaan}</Title>
              </div>
          </>
        ),
        function: (
          <>
            <div className="author-info">
              <Title level={5}>{d.alamat}</Title>
              <p>Organization</p>
            </div>
          </>
        ),

        provinsi: (
          <>
            <div className="author-info">
              <Title level={5}>{d.provinsi}</Title>
            </div>
          </>
        ),

        kabkota: (
          <>
            <div className="author-info">
              <Title level={5}>{d.kabkota}</Title>
            </div>
          </>
        ),
    
        status: (
          <>
          <p>Score: {d.score}</p>
          {d.score < 66
            ?
              <Tag color="error">LABEL MERAH</Tag>
            : [
              d.score > 65 && d.score < 80
              ?
              <Tag color="warning">LABEL KUNING</Tag>
              :
              <Tag color="success">LABEL HIJAU</Tag>
            ]
          }
          </>
        ),
        waktu_ujian: (
          <>
            <div className="ant-employed">
              <span>{moment(d.penilaian.waktu_selesai).format("D MMMM YYYY")}</span>
            </div>
          </>
        ),
      }
    ))

    const downloadPDF = () => {
      const doc = new jsPDF();
      doc.text("Data Detail",20,10);
      doc.save('perusahaan.pdf');
      doc.autoTable({
        columns: columns.map(col=>({...col, dataKey: col.dataIndex})),
        body: data
      })
    }

    const downloadExcel = () => {
        const workSheet = XLSX.utils.json_to_sheet(get_data)
        const workBook = XLSX.utils.book_new()
        XLSX.utils.book_append_sheet(workBook, workSheet, "Perusahaan")
        let buf = XLSX.write(workBook,{bookType: "xlsx", type: "buffer"})
        XLSX.write(workBook,{bookType: "xlsx", type: "binary"})
        XLSX.writeFile(workBook,"perusahaan.xlsx")
    }

    const handleChange = (...sorted) => {
        const {order,field} = sorted[2];
        setSortedInfo({columnKey: field, order})
    }

    const handleInputChange = (e) => {
      setCari(e.target.value)
      if(e.target.value === "") {
          dataCompany();
      } 
    };

    const globalSearch = () => {
        filteredData = modifiedData.filter((value) => {
            return(
                value.nama_perusahaan.toLowerCase().includes(cari.toLowerCase()) ||
                value.alamat.toLowerCase().includes(cari.toLowerCase()) ||
                value.provinsi.toLowerCase().includes(cari.toLowerCase()) ||
                value.kabkota.toLowerCase().includes(cari.toLowerCase())
            );
        });
        setGetData(filteredData)
    };
  
    return (
      <>
        <div className="tabled">
          <Row gutter={[24, 0]}>
            <Col xs="24" xl={24}>
              <Card
                bordered={false}
                className="criclebox tablespace mb-24"
                title="Tabel Perusahaan"
                extra={
                  <>
                    <Input style={{ width: 280 }} placeholder="Cari..." 
                      type="text"
                      onChange={handleInputChange}
                      value={cari}
                      /> 
                    <Radio.Group onChange={onChange} defaultValue="a">
                      <Button onClick={globalSearch} type="primary">Cari</Button>
                      <Button onClick={downloadPDF}>PDF</Button>
                      <Button onClick={downloadExcel} success>Excel</Button>
                    </Radio.Group>
                  </>
                }
              >
                <div className="table-responsive">
                  <Table
                    columns={columns}
                    dataSource={filteredData && filteredData.length ? filteredData : data}
                    loading={loading}
                    onChange={handleChange}
                    pagination={{
                        pageSize: 10,
                        total: totalPages,
                        onChange: (page) => {
                          dataCompany(page);
                        },
                    }}
                    className="ant-border-space"
                  />
                </div>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
  
  export default Perusahaan;
  