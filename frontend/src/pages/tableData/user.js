import { Table, Card, Col, Row } from 'antd';
import qs from 'qs';
import React, { useEffect, useState } from 'react';
import { endpoint } from '../../utils/endpoint';
import axios from 'axios';

const columns = [
  {
    title: 'Username',
    dataIndex: 'username',
    sorter: true,
    width: '20%',
  },
  {
    title: 'Password',
    dataIndex: 'password',
    width: '20%',
  },
  {
    title: 'Level',
    dataIndex: 'level',
  },
];

const User = () => {
  const [data, setData] = useState();
  const [totalPages, setTotalPages] = useState(1);
  const [loading, setLoading] = useState(false);
  const fetchData = async (page) => {
    setLoading(true);
    await axios.get(`${endpoint}/user/?offset=${page}&limit=10`)
      .then((res) => {
        setData(res.data);
        setTotalPages(res.data.totalPages);
        setLoading(false);
        console.log(res.data);
      });
  };

  useEffect(() => {
    fetchData(0);
  }, [])

  return (
    <div className="tabled">
         <Row gutter={[24, 0]}>
            <Col xs="24" xl={24}>
            <Card
              bordered={false}
              className="criclebox tablespace mb-24"
              title="Tabel User"
            >
            <div className='table-responsive'>
                <Table
                columns={columns}
                dataSource={data}
                loading={loading}
                pagination={{
                    pageSize: 10,
                    total: totalPages,
                    onChange: (page) => {
                      fetchData(page);
                    },
                }}
                />
            </div>
            </Card>
            </Col>
         </Row>
    </div>
  );
};
export default User;