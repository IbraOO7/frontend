/*!
  =========================================================
  * Muse Ant Design Dashboard - v1.0.0
  =========================================================
  * Product Page: https://www.creative-tim.com/product/muse-ant-design-dashboard
  * Copyright 2021 Creative Tim (https://www.creative-tim.com)
  * Licensed under MIT (https://github.com/creativetimofficial/muse-ant-design-dashboard/blob/main/LICENSE.md)
  * Coded by Creative Tim
  =========================================================
  * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import ReactApexChart from "react-apexcharts";
import { Row, Col, Typography, Button, Form, Select } from "antd";
import eChart from "./configs/eChart";
import React, {useState, useEffect} from 'react';
import axios from "axios";
import { endpoint } from "../../utils/endpoint";

function EChart() {
  const { Title, Paragraph } = Typography;
  const [datas, setDatas] = useState([]);
  const [provinsi, setProvinsi] = useState([]);
  const [loading, setLoading] = useState(false);

  const getProvinsi = async () => {
    await axios.get(`${endpoint}/lokasi/`)
    .then((res) => {
      setProvinsi(res.data.data);
      console.log(res.data.data);
    }).catch(err => console.log(err));
  };

  useEffect(() => {
    getProvinsi();
  }, []);

  const getData = async (e) => {
      setLoading(true);
      await axios.get(`${endpoint}/data_driven/statistika/${e}`)
      .then((res) => {
        setLoading(false);
        setDatas(res.data)
        console.log(res.data)
      }).catch(err => console.log(err))
  }

  useEffect(() => {
    getData();
  }, []);

  const getAll = async () => {
    setLoading(true);
    await axios.get(`${endpoint}/data_driven/label_statistik`)
    .then((res) => {
      setLoading(false);
      setDatas(res.data);
      console.log(res.data);
    }).catch(err => console.log(err));
  };

  useEffect(() => {
      getAll();
  }, []);

  const sum = datas.reduce((accumulator, value) => {
    return accumulator + value;
  }, 0);

  const items = [
    {
      Title: sum,
      user: "Keseluruhan",
    },
    {
      Title: datas[0],
      user: "Berlabel Hijau",
    },
    {
      Title: datas[1],
      user: "Berlabel Kuning",
    },
    {
      Title: datas[2],
      user: "Berlabel Merah",
    },
  ];

  const { Option } = Select;

  return (
    <>
      <div id="chart">
        <ReactApexChart
          options={eChart.options}
          series={ 
          [{
            data: datas
          }]

          }
          type="bar"
          height={250}
          loading={loading}
        />
      </div>
      <div className="chart-vistior">
        <Title level={5}>Data Keseluruhan</Title>
        <Row gutter>
          {items.map((v, index) => (
            <Col xs={6} xl={6} sm={6} md={6} key={index}>
              <div className="chart-visitor-count">
                <Title level={4}>{v.Title}</Title>
                <span>{v.user}</span>
              </div>
            </Col>
          ))}
        </Row>
      </div>
      <br />
      <Form name="horizontal_login" layout="inline" >
      <Form.Item
        name="Provinsi"
        style={{width: "180px"}}
      >
      <Select onChange={e => getData(e)}>
        {provinsi.map((data) => (
            <Option value={data.name}>{data.name}</Option>
        ))}
      </Select>
      </Form.Item>
    </Form>
    </>
  );
}

export default EChart;
